rootProject.name = "Exploring Porject Problems"

include("explore-mssql-java",
        "explore-spring-rest",
        "explore-file-uploader",
        "explore-file-handler",
        "explore-spring-call",
        "explore-swagger",
        "explore-spring-prototype")