import org.ajoberstar.grgit.Grgit


plugins {
    java
    idea
    id("org.springframework.boot") version "2.2.5.RELEASE" apply false
    id("org.ajoberstar.grgit") version "3.0.0"
}

repositories {
    mavenCentral()
}

val git: Grgit = Grgit.open(hashMapOf("dir" to rootProject.rootDir) as Map<String, Any>?)
val commitId: String = git.head().id
val branch = if (project.hasProperty("branch")) project.properties["branch"] else git.branch.current().name
logger.lifecycle("On branch $branch, commitId $commitId")


allprojects {
    repositories {
        mavenCentral()
    }

    apply(plugin = "java")
    apply(plugin = "groovy")
    apply(plugin = "io.spring.dependency-management")

    java {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

subprojects {
    apply(plugin = "org.springframework.boot")
    apply(plugin = "java")

    dependencies {
        implementation("org.springframework.boot:spring-boot-starter-web")
        implementation("org.springframework.boot:spring-boot-starter-actuator")

        implementation("com.fasterxml.jackson.datatype:jackson-datatype-joda:2.9.8")
        implementation("com.fasterxml.jackson.datatype:jackson-datatype-guava:2.9.8")

        implementation("com.google.guava:guava:21.0")
        compileOnly("org.projectlombok:lombok:1.18.4")
        implementation("joda-time:joda-time:2.10.1")

        testImplementation("org.codehaus.groovy:groovy-all:2.4.13")
        testImplementation("org.spockframework:spock-core:1.0-groovy-2.4")
        testImplementation("org.spockframework:spock-spring:1.0-groovy-2.4")
    }

    tasks.named("jar", Jar::class) {
        manifest {
            attributes("Implementation-Title" to project.name,
                    "Implementation-Version" to version,
                    "Git-Branch" to branch,
                    "Git-Commit" to commitId)
        }
    }
}

