package com.rmurugaian.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author rmurugaian 2019-01-04
 */
@SpringBootApplication
public class SpringRestApplication {
    public static void main(final String[] args) {
        SpringApplication.run(SpringRestApplication.class, args);
    }
}
