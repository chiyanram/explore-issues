package com.rmurugaian.spring;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

/**
 * @author rmurugaian 2019-01-04
 */
@RestController
public class ErrorRestController {

    @GetMapping("/errorCase")
    public ResponseEntity<String> error() {

        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/exceptionCase")
    public void exception() {
        throw new HttpServerErrorException(HttpStatus.BAD_REQUEST);
    }
}
