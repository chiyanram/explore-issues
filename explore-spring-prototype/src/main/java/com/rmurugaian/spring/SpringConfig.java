package com.rmurugaian.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.stream.IntStream;

/**
 * @author rmurugaian 2019-04-05
 */
@Configuration
@ComponentScan
public class SpringConfig {

    @Resource
    private Singleton singletonOne;

    @Resource
    private Singleton singletonTwo;

    @Resource
    private Prototype prototype;

    public void print() {

        IntStream.of(1, 2)
            .forEach(value -> System.out.println(singletonOne.getPrototype().hashCode()));

        System.out.println(singletonTwo.getPrototype().hashCode());

        System.out.println(prototype.hashCode());

    }


    @Bean(name = "doc")
    public Doctor doctor(){
        return new Doctor("Ram","Card");
    }

}
