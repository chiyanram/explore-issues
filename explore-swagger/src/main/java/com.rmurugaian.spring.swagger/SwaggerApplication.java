package com.rmurugaian.spring.swagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author rmurugaian 2019-02-19
 */
@SpringBootApplication
public class SwaggerApplication {

    public static void main(final String[] args) {
        SpringApplication.run(SwaggerApplication.class, args);
    }
}
